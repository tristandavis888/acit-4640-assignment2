module "vpc" {
  source       = "./modules/terraform_vpc"
  project_name = var.project_name
}


module "ssh_key" {
  source = "./modules/terraform_ssh"
  key_name = var.ssh_key_name
}


module "ec2" {
  source = "./modules/terraform_ec2"
  project_name = var.project_name
  aws_region = var.aws_region
  ami_id = var.ami_id
  subnet_id = [module.vpc.subnet_1_id, module.vpc.subnet_2_id]
  security_group_id = module.vpc.security_group_id
  ssh_key_name = var.ssh_key_name
}