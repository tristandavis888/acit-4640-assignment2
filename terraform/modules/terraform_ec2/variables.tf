variable "project_name" {
  description = "ACIT 4640 Assignment 1"
}

variable "ami_id" {
  description = "AMI for the server"
}

variable "instance_type" {
  description = "Instance type"
  default = "t2.micro"
}

variable "ssh_key_name" {
  description = "AWS SSH key name"
  default     = "/home/munib/ass2/acit-4640-assignment2/terraform/acit_4640_202330.pem"
}

variable "aws_region" {
  description = "AWS region"
}

variable "availability_zones" {
  description = "AWS availability zone"
  default = ["us-west-2a", "us-west-2b"]
}

variable "subnet_id" {
    description = "Subnet ID"
}

variable "security_group_id" {
    description = "Security Group ID"
}




