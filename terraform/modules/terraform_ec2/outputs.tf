output "instance_ids_1" {
  description = "IDs of the first EC2 instances"
  value       = aws_instance.EC2_instance[0].id
}
output "instance_id_2" {
description  = "IDS of second EC2 Instance"
value = aws_instance.EC2_instance[1].id
}

output "EC2_Public_1" {
  description = "Public IP addresses of first instance"
  value       = aws_instance.EC2_instance[0].public_ip
}
output "EC2_Public_2" {
  description = "Public IP address of second instance"
  value = aws_instance.EC2_instance[1].public_ip
}


