provider "aws" {
    region = var.aws_region
}

resource "aws_vpc" "vpc_1" {
    cidr_block           = var.vpc_cidr
    enable_dns_support   = true
    enable_dns_hostnames = true
    tags = {
        Name    = var.vpc_name
        Project = var.project_name 
    }
}

resource "aws_subnet" "subnet_1" {
    vpc_id            = aws_vpc.vpc_1.id
    cidr_block        = var.subnet_cidr_1
    availability_zone = "us-west-2a"
    map_public_ip_on_launch = true
    tags = {
        Name    = var.subnet_name
        Project = var.project_name
    }
}

resource "aws_subnet" "subnet_2" {
    vpc_id            = aws_vpc.vpc_1.id
    cidr_block        = var.subnet_cidr_2
    availability_zone = "us-west-2b"
    map_public_ip_on_launch = true
    tags = {
        Name    = var.subnet_name
        Project = var.project_name
    }
}

resource "aws_internet_gateway" "igw_1" {
    vpc_id = aws_vpc.vpc_1.id
    tags = {
        Name    = "Internet Gateway"
        Project = var.project_name
    }
}

resource "aws_route_table" "route_table_1" {
    vpc_id = aws_vpc.vpc_1.id

    route {
        cidr_block =  var.default_route
        gateway_id = aws_internet_gateway.igw_1.id
    }

    tags = {
        Name    = "Route Table"
        Project = var.project_name
    }
}

resource "aws_route_table_association" "route_table_association_1" {
    subnet_id      = aws_subnet.subnet_1.id
    route_table_id = aws_route_table.route_table_1.id
}

resource "aws_route_table_association" "route_table_association_2" {
    subnet_id      = aws_subnet.subnet_2.id
    route_table_id = aws_route_table.route_table_1.id
}

# Security Group Creation

resource "aws_security_group" "security_group" {
    name        = "security_group"
    description = "Allows all http and ssh in from within the VPC"
    vpc_id      = aws_vpc.vpc_1.id  
}

resource "aws_vpc_security_group_egress_rule" "egress_rule" {
    security_group_id = aws_security_group.security_group.id
    ip_protocol          = -1
    cidr_ipv4 =         var.default_route
    description       = "Allow all egress traffic"
}

resource "aws_vpc_security_group_ingress_rule" "ssh_rule" {
    security_group_id = aws_security_group.security_group.id
    from_port         = 22
    to_port           = 22
    ip_protocol          = "tcp"
    cidr_ipv4       = var.ingress_cidr
    description       = "Allow SSH access"
}

resource "aws_vpc_security_group_ingress_rule" "http_rule" {
    security_group_id = aws_security_group.security_group.id
    from_port         = 80
    to_port           = 80
    ip_protocol          = "tcp"
    cidr_ipv4       = var.ingress_cidr
    description       = "Allow HTTP access"
}
